function showMessage(idThongBao, noidung) {
  if (noidung == "") {
    domID(idThongBao).style.display = "none";
  } else {
    domID(idThongBao).style.display = "block";
  }
  domID(idThongBao).innerHTML = noidung;
}

function kiemTraDoDai(id, min, max, idthongbao, noidungTB) {
  let kiemTra = id;
  if (kiemTra.length >= min && kiemTra.length <= max) {
    showMessage(idthongbao, "");
    return true;
  } else {
    showMessage(idthongbao, noidungTB);
    return false;
  }
}

function kiemTraKiTuChu(noidungcancheck, idThongBao) {
  let letters = /^[A-Z a-z]+$/;
  if (letters.test(noidungcancheck)) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(idThongBao, "Vui lòng chỉ nhập kí tự chữ");
    return false;
  }
}
function kiemTraKiSo(noidungcancheck, idThongBao) {
  let letters = /^[0-9]+$/;
  if (letters.test(noidungcancheck)) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(idThongBao, "Vui lòng chỉ nhập kí tự số");
    return false;
  }
}
function kiemTraEmail(noidungcancheck, idThongBao) {
  let letters =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (letters.test(noidungcancheck)) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(idThongBao, "Vui lòng nhập đúng định dạng email");
    return false;
  }
}

function kiemTraPass(noidungcancheck, idThongBao) {
  let letters =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/;
  if (letters.test(noidungcancheck)) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(
      idThongBao,
      "Mật khẩu bao gồm 6-10 kí tự (chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt"
    );
    return false;
  }
}

function kiemTraNhapLuong(noidungcancheck, idThongBao) {
  if (noidungcancheck >= 1000000 && noidungcancheck <= 20000000) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(idThongBao, "Lương căn bản phải từ 1.000.000 - 20.000.000");
  }
}

function kiemTraNgayLam(noidungcancheck, idThongBao) {
  if (noidungcancheck == "") {
    showMessage(idThongBao, "Vui lòng nhập ngày làm việc");
    return false;
  } else {
    showMessage(idThongBao, "");
    return true;
  }
}

function kiemTraChucVu(noidungcancheck, idThongBao) {
  if (noidungcancheck == "Chọn chức vụ") {
    showMessage(idThongBao, "Vui lòng chọn chức vụ");
    return false;
  } else {
    showMessage(idThongBao, "");
    return true;
  }
}

function kiemTraGioLam(noidungcancheck, idThongBao) {
  if (noidungcancheck >= 80 && noidungcancheck <= 200) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(idThongBao, "Giờ làm phải từ 80-200 giờ");
  }
}

function kiemTraTrung(idThongBao, danhSachNhanVien, nv) {
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == nv.taiKhoan;
  });
  if (viTri == -1) {
    showMessage(idThongBao, "");
    return true;
  } else {
    showMessage(idThongBao, "Dữ liệu đã tồn tại");
    return false;
  }
}
