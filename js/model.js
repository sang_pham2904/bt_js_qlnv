function NhanVien(
  taiKhoan,
  hoTen,
  eMail,
  matKhau,
  ngayLam,
  luongCoban,
  chucVu,
  indexChucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoTen = hoTen;
  this.eMail = eMail;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoban = luongCoban;
  this.chucVu = chucVu;
  this.indexChucVu = indexChucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCoban * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongCoban * 2;
    } else {
      return this.luongCoban * 1;
    }
  };
  this.xepLoai = function () {
    if (this.chucVu == "Sếp") {
      return "Sếp";
    } else if (this.chucVu == "Trưởng phòng") {
      return "Trưởng phòng";
    } else if (this.chucVu == "Nhân viên" && this.gioLam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.chucVu == "Nhân viên" && this.gioLam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.chucVu == "Nhân viên" && this.gioLam >= 160) {
      return "Nhân viên khá";
    } else if (this.chucVu == "Nhân viên" && this.gioLam < 160) {
      return "Nhân viên trung bình";
    }
  };
}
