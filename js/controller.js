function domID(id) {
  return document.getElementById(id);
}
function layTTTuForm() {
  let taiKhoan = domID("tknv").value;
  let hoTen = domID("name").value;
  let eMail = domID("email").value;
  let matKhau = domID("password").value;
  let ngayLam = domID("datepicker").value;
  let luongCoban = Number(domID("luongCB").value);
  let chucVu = domID("chucvu").options[domID("chucvu").selectedIndex].text;
  let indexChucVu = domID("chucvu").selectedIndex;
  let gioLam = Number(domID("gioLam").value);
  //   lấy thông tin trên nhập vào obj
  let nv = new NhanVien(
    taiKhoan,
    hoTen,
    eMail,
    matKhau,
    ngayLam,
    luongCoban,
    chucVu,
    indexChucVu,
    gioLam
  );
  return nv;
}
function render(danhSachNhanVien) {
  let NhanVien = "";
  for (let i = 0; i < danhSachNhanVien.length; i++) {
    NhanVien += `<tr>
                        <td>                        
                        ${danhSachNhanVien[i].taiKhoan}                       
                        </td>
                        <td>
                            ${danhSachNhanVien[i].hoTen}
                        </td>
                        <td>
                            ${danhSachNhanVien[i].eMail}
                        </td>
                        <td>
                            ${danhSachNhanVien[i].ngayLam}
                        </td>
                        <td>
                            ${danhSachNhanVien[i].chucVu}
                        </td>
                        <td>
                            ${danhSachNhanVien[i].tongLuong()}
                        </td>
                        <td>
                            ${danhSachNhanVien[i].xepLoai()}
                        </td>
                        <td>
                            <button class = "btn btn-danger" onclick="xoaNV('${
                              danhSachNhanVien[i].taiKhoan
                            }')">Xóa
                            </button>
                            <button class = "btn btn-primary" data-toggle="modal"
                            data-target="#myModal" onclick="suaNV('${
                              danhSachNhanVien[i].taiKhoan
                            }')">Sửa
                            </button>
                        </td>
                    </tr>`;
  }
  domID("tableDanhSach").innerHTML = NhanVien;
}

function resetForm() {
  document.querySelector(".modal-body form").reset();
  let thongbao = document.querySelectorAll(".sp-thongbao");
  var arrThongBao = [];
  for (let i = 0; i < thongbao.length; i++) {
    arrThongBao.push(thongbao[i]);
  }
  for (let j = 0; j < arrThongBao.length; j++) {
    arrThongBao[j].style.display = "none";
  }
}
