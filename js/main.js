let danhSachNhanVien = [];
const DSNV_LOCAL = "DSNV_LOCAL";
let JSONdata = localStorage.getItem(DSNV_LOCAL);
if (JSONdata != null) {
  danhSachNhanVien = JSON.parse(JSONdata).map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.eMail,
      item.matKhau,
      item.ngayLam,
      item.luongCoban,
      item.chucVu,
      item.indexChucVu,
      item.gioLam
    );
  });
  render(danhSachNhanVien);
}

// ***********************thêm nhân viên *****************************
domID("btnThem").onclick = function () {
  resetForm();
};
domID("btnThemNV").addEventListener("click", function () {
  let nv = layTTTuForm();
  //   kiểm tra mail
  let isValid = kiemTraEmail(nv.eMail, "tbEmail");
  //   kiểm tra mật khẩu
  isValid &= kiemTraPass(nv.matKhau, "tbMatKhau");
  // Kiểm tra tên
  isValid &= kiemTraKiTuChu(nv.hoTen, "tbTen");
  // Kiểm tra tài khoản
  isValid &=
    kiemTraDoDai(
      nv.taiKhoan,
      4,
      6,
      "tbTKNV",
      "Vui lòng nhập tài khoản từ 4-6 kí số"
    ) &&
    kiemTraKiSo(nv.taiKhoan, "tbTKNV") &&
    kiemTraTrung("tbTKNV", danhSachNhanVien, nv);
  isValid &= kiemTraNhapLuong(nv.luongCoban, "tbLuongCB");
  // kiểm tra ngày làm việc
  isValid &= kiemTraNgayLam(nv.ngayLam, "tbNgay");
  // kiểm tra chức vụ
  isValid &= kiemTraChucVu(nv.chucVu, "tbChucVu");
  // kiểm tra giờ làm
  isValid &= kiemTraGioLam(nv.gioLam, "tbGiolam");

  if (isValid) {
    danhSachNhanVien.push(nv);
    render(danhSachNhanVien);
    let dataJSON = JSON.stringify(danhSachNhanVien);
    localStorage.setItem(DSNV_LOCAL, dataJSON);
    resetForm();
  }
});
// *************************Xóa nhân viên **********************************
// Xóa nhân viên
function xoaNV(soTaiKhoan) {
  let viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == soTaiKhoan;
  });
  danhSachNhanVien.splice(viTri, 1);
  render(danhSachNhanVien);
  let dataJSON = JSON.stringify(danhSachNhanVien);
  localStorage.setItem(DSNV_LOCAL, dataJSON);
}

// ****************************Cập nhât*******************************************
function suaNV(soTaiKhoan) {
  let viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == soTaiKhoan;
  });
  domID("tknv").value = danhSachNhanVien[viTri].taiKhoan;
  domID("name").value = danhSachNhanVien[viTri].hoTen;
  domID("email").value = danhSachNhanVien[viTri].eMail;
  domID("password").value = danhSachNhanVien[viTri].matKhau;
  domID("datepicker").value = danhSachNhanVien[viTri].ngayLam;
  domID("luongCB").value = danhSachNhanVien[viTri].luongCoban;
  domID("chucvu").selectedIndex = danhSachNhanVien[viTri].indexChucVu;
  domID("gioLam").value = danhSachNhanVien[viTri].gioLam;
  domID("tknv").disabled = true;
  // domID("myModal").classList.add("show");
  // domID("myModal").style.display = "block";
}

domID("btnCapNhat").onclick = function () {
  let nv = layTTTuForm();
  let isValid = kiemTraEmail(nv.eMail, "tbEmail");
  //   kiểm tra mật khẩu
  isValid &= kiemTraPass(nv.matKhau, "tbMatKhau");
  // Kiểm tra tên
  isValid &= kiemTraKiTuChu(nv.hoTen, "tbTen");
  // Kiểm tra tài khoản
  isValid &=
    kiemTraDoDai(
      nv.taiKhoan,
      4,
      6,
      "tbTKNV",
      "Vui lòng nhập tài khoản từ 4-6 kí số"
    ) && kiemTraKiSo(nv.taiKhoan, "tbTKNV");
  isValid &= kiemTraNhapLuong(nv.luongCoban, "tbLuongCB");
  // kiểm tra ngày làm việc
  isValid &= kiemTraNgayLam(nv.ngayLam, "tbNgay");
  // kiểm tra chức vụ
  isValid &= kiemTraChucVu(nv.chucVu, "tbChucVu");
  // kiểm tra giờ làm
  isValid &= kiemTraGioLam(nv.gioLam, "tbGiolam");

  if (isValid) {
    let viTri = danhSachNhanVien.findIndex(function (item) {
      return item.taiKhoan == layTTTuForm().taiKhoan;
    });
    danhSachNhanVien[viTri] = layTTTuForm();
    render(danhSachNhanVien);
    let dataJSON = JSON.stringify(danhSachNhanVien);
    localStorage.setItem(DSNV_LOCAL, dataJSON);
    domID("myModal").classList.remove("show");
    domID("myModal").style.display = "none";
    domID("tknv").disabled = false;
  }
};
// ****************************** Đóng modal**************************************
domID("btnDong").onclick = function () {
  resetForm();
  domID("myModal").classList.remove("show");
  domID("myModal").style.display = "none";
  domID("tknv").disabled = false;
};
// window.onclick = function (event) {
//   let modal = domID("myModal");
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// };

// *************** Search***************************************************
console.log(danhSachNhanVien);
domID("btnTimNV").addEventListener("click", function () {
  let inputSearch = domID("searchName").value;
  let filterXepLoai = danhSachNhanVien.filter(function (item) {
    return item.xepLoai().toLowerCase().includes(inputSearch.toLowerCase());
  });
  if (filterXepLoai.length > 0) {
    render(filterXepLoai);
  } else {
    domID("tableDanhSach").innerHTML = `<tr>
    <td colspan = '8'>
    <h3 class = 'text-warning'>Không tìm thấy thông tin <span class= 'text-danger'>"${inputSearch}"</span></h3>
    <p class= 'text-primary'>Lưu ý: nhập tiếng Việt có dấu</p>
    </td>
    </tr>`;
  }
  // console.log(filterXepLoai);
});

let inputSearch = domID("searchName");
inputSearch.addEventListener("keyup", function (event) {
  if (event.key == "Enter") {
    event.preventDefault();
    domID("btnTimNV").click();
  }
});
